# Credits

## Maintainers

* Clément Elvira <clement.elvira@centralesupelec.fr>
* Cédric Herzet <cedric.herzet@inria.fr>

## Contributors

None yet. Why not be the first? See: CONTRIBUTING.md
    
